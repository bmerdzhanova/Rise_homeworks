﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci
{
    public class FibonacciSquare
    {
        public static bool hasFibSeqInSquare(int[,] matrix)
        {
            int n = matrix.GetLength(0);
            for (int len = 2; len <= n; len++)
            {
                for (int i = 0; i <= n - len; i++)
                {
                    for (int j = 0; j <= n - len; j++)
                    {
                        if (IsFibonacciSquare(matrix, i, j, len))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public static bool IsFibonacci(int a, int b, int n)
        {
            int c = a + b;
            while (n > 2 && c <= n)
            {
                a = b;
                b = c;
                c = a + b;
            }
            if (n == 2)
            {
                return c == b + a;
            }
            return c == n;
        }

        private static bool IsFibonacciSquare(int[,] matrix, int i, int j, int len)
        {
            // Check top border
            if (!IsFibonacci(matrix[i, j], matrix[i, j + len - 1], len - 1))
            {
                return false;
            }

            // Check right border
            if (!IsFibonacci(matrix[i, j + len - 1], matrix[i + len - 1, j + len - 1], len - 1))
            {
                return false;
            }

            // Check bottom border
            if (!IsFibonacci(matrix[i + len - 1, j + len - 1], matrix[i + len - 1, j], len - 1))
            {
                return false;
            }

            // Check left border
            if (!IsFibonacci(matrix[i + len - 1, j], matrix[i, j], len - 1))
            {
                return false;
            }

            // Check corners
            if (!IsFibonacci(matrix[i, j], matrix[i, j + len - 2], len - 2))
            {
                return false;
            }
            if (!IsFibonacci(matrix[i, j + len - 2], matrix[i + len - 2, j + len - 1], len - 2))
            {
                return false;
            }
            if (!IsFibonacci(matrix[i + len - 2, j + len - 1], matrix[i + len - 1, j + 1], len - 2))
            {
                return false;
            }
            if (!IsFibonacci(matrix[i + len - 1, j + 1], matrix[i + 1, j], len - 2))
            {
                return false;
            }

            return true;
        }

        public static void Main(string[] args)
        {
            int[,] matrix = { { 1, 1, 5 }, { 21, 4, 3 }, { 13, 8, 5 } };
            bool result = hasFibSeqInSquare(matrix);
            Console.WriteLine(result);
        }
    }
}
