﻿using System;
using System.Runtime;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace SmallTasks
{

    public class NumberTasks
    {
        public bool IsOdd(int n)
        {
            return n % 2 != 0;
        }

        public bool IsPrime(int n)
        {
            if (n <= 1) return false;
            if (n == 2) return true;
            if (n % 2 == 0) return false;

            var boundary = (int)Math.Floor(Math.Sqrt(n));

            for (int i = 3; i <= boundary; i += 2)
                if (n % i == 0)
                    return false;

            return true;

        }

        public static int MinElement(int[] array)
        {
            if (array.Length == 0) return 0;
            int minElement = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < minElement)
                {
                    minElement = array[i];
                }
            }
            return minElement;
        }

        public static int KthMin(int k, int[] array)
        {
            if (k <= 0 || k > array.Length)
            {
                throw new ArgumentException("Invalid k value");
            }

            Array.Sort(array);

            return array[k - 1];
        }

        public static int GetOddOccurrence(int[] array)
        {
            int size = array.Length;
            for (int i = 0; i < size; ++i)
            {
                int count = 0;
                for (int j = 0; j < size; j++)
                {
                    if (array[i] == array[j])
                        count++;
                }
                if (count % 2 != 0)
                    return array[i];
            }
            return -1;
        }

        public static double GetAverage(int[] array)
        {
            if (array.Length == 0)
            {
                throw new ArgumentException("Array must not be empty.");
            }
            double sum = 0;
            foreach (int element in array)
            {
                sum += element;
            }

            return sum / array.Length;
        }

        public static long Pow(int a, int b)
        {
            if (b == 0)
                return 1;
            long result = Pow(a, b / 2);
            if (b % 2 == 0)
            {
                return result * result;
            }
            else
            {
                return result * result * a;
            }
        }

        public static long GetSmallestMultiple(int N)
        {
            long result = 1;
            for (int i = 2; i <= N; i++)
            {
                result = LCM(result, i);
            }
            return result;
        }

        public static long LCM(long a, long b)
        {
            return (a * b) / GCD(a, b);
        }

        public static long GCD(long a, long b)
        {
            if (b == 0)
                return a;
            return GCD(b, a % b);
        }

        public static long DoubleFact(int n)
        {
            if (n <= 1)
            {
                return 1;
            }
            return n * DoubleFact(n - 2);
        }

        public static long KthFac(int k, int n)
        {
            if (n == 0 || n == 1)
            {
                return 1;
            }
            long result = n;
            for (int i = n - k; i > 1; i -= k)
            {
                result *= i;
            }
            return result;
        }

        public static void Main(string[] args)
        {
            short maxShort = short.MaxValue;
            short maxShortPlusOne = (short)(maxShort + 1);
            //Console.WriteLine("Maximum value of type short: " + maxShort);
            //Console.WriteLine("Maximum value of short plus one: " + maxShortPlusOne);

            int maxInt = int.MaxValue;
            int maxIntPlusOne = (int)(maxInt + 1);
            //Console.WriteLine("Maximum value of type int: " + maxInt);
            //Console.WriteLine("Maximum value of int plus one: " + maxIntPlusOne);

            double maxDouble = double.MaxValue;
            double maxDoublePlusOne = (double)(maxDouble + 1);
            //Console.WriteLine("Maximum value of type short: " + maxDouble);
            //Console.WriteLine("Maximum value of short plus one: " + maxDoublePlusOne);

            float maxFloat = float.MaxValue;
            float maxFloatPlusOne = (float)(maxFloat + 1);
            //Console.WriteLine("Maximum value of type short: " + maxFloat);
            //Console.WriteLine("Maximum value of short plus one: " + maxFloatPlusOne);

            long maxLong = long.MaxValue;
            long maxLongPlusOne = (long)(maxLong + 1);
            //Console.WriteLine("Maximum value of type short: " + maxLong);
            //Console.WriteLine("Maximum value of short plus one: " + maxLongPlusOne);

            decimal maxDecimal = decimal.MaxValue;
            decimal maxDecimalPlusOne = (decimal)(maxDecimal + 1);
            //Console.WriteLine("Maximum value of type short: " + maxDecimal);
            //Console.WriteLine("Maximum value of short plus one: " + maxDecimalPlusOne);
        }
    }
}