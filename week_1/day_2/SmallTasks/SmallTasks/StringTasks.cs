﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallTasksString
{
    public class StringTasks
    {
        public static string Reverse(string InputString)
        {
            string result = "";
            for (int i = InputString.Length - 1; i >= 0; --i)
            {
                result += InputString[i];
            }
            return result;

        }
        public static void Main(String[] args)
        { }
    }
    
}
