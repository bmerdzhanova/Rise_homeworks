using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmallTasks;
namespace TestSmallTasks
{
    [TestClass]
    public class TestIsPrime
    {
        [TestMethod]
        public void IsPrime_WhenNumberIsPrime_ReturnsTrue()
        {
            // Arrange
            var program = new NumberTasks();

            // Act
            var result = program.IsPrime(3);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void isPrime_WhenNumberIsLessThanOne_ReturnFalse()
        {
            // Arrange
            var program = new NumberTasks();

            // Act
            var result = program.IsPrime(-2);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsPrime_WhenNumberIsEven_ReturnsFalse()
        {
            // Arrange
            var program = new NumberTasks();

            // Act
            var result = program.IsPrime(4);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsPrime_WhenNumberIsNotPrime_ReturnsFalse()
        {
            // Arrange
            var program = new NumberTasks();

            // Act
            var result = program.IsPrime(21);

            // Assert
            Assert.IsFalse(result);
        }



    }
}