﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmallTasks;


namespace TestSmallTasks
{
    [TestClass]
    public class TestKthMin
    {
        [TestMethod]
        public void KthMin_ReturnsKthSmallestElement()
        {
            int[] array = { 3, 1, 4, 1, 5, 9, 2, 6, 5, 3 };
            int k = 4;
            int expected = 3;

            int actual = NumberTasks.KthMin(k, array);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void KthMin_ReturnsKthSmallesElementInArrayWithOneElement()
        {
            int[] array = { 1 };
            int k = 1;
            int expected = 1;
            int actual = NumberTasks.KthMin(k, array);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void KthMin_ReturnsKthSmallestElementInArrayWithZeroElement()
        {
            int[] array = { };
            int k = 4;

            Assert.ThrowsException<ArgumentException>(() => NumberTasks.KthMin(k, array));
        }

        [TestMethod]
        public void KthMin_WhenKIsNotCorrect_ReturnException()
        {
            int[] array = { 1, 2, 3, 4, 5 };
            int k = 7;

            Assert.ThrowsException<ArgumentException>(() => NumberTasks.KthMin(k, array));
        }

    }
}
