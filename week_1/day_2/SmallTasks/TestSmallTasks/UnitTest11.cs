﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmallTasks;


namespace TestSmallTasks
{
    [TestClass]
    public class TestKthFac
    {
        [TestMethod]
        public void KthFac()
        {
            Assert.AreEqual(1, NumberTasks.KthFac(3, 0));
            Assert.AreEqual(1, NumberTasks.KthFac(3, 1));
            Assert.AreEqual(3, NumberTasks.KthFac(3, 3));
            Assert.AreEqual(10, NumberTasks.KthFac(3, 5));
            Assert.AreEqual(10, NumberTasks.KthFac(10, 10));
        }
    }
}
