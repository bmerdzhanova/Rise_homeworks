﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmallTasks;


namespace TestSmallTasks
{
    [TestClass]
    public class TestGetSmallestMultiple
    {
        [TestMethod]
        public void GetSmallestMultiple_WhenNumberIsLegit()
        {
            int n = 5;
            int expected = 60;
            long actual = NumberTasks.GetSmallestMultiple(n);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void LCM_WhenNumbersAreLegit()
        {
            long a = 6;
            long b = 8;
            int expected = 24;
            long actual = NumberTasks.LCM(a, b);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GCD_WhenNumbersAreLegit()
        {
            long a = 12;
            long b = 18;
            int expected = 6;
            long actual = NumberTasks.GCD(a, b);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetSmallestMultiple_WhenNumberIsZero()
        {
            int n = 0;
            int expected = 1;
            long actual = NumberTasks.GetSmallestMultiple(n);
            Assert.AreEqual(expected, actual);
        }
    }
}
