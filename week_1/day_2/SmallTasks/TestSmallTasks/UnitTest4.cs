﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmallTasks;


namespace TestSmallTasks
{
    [TestClass]
    public class TestMinElement
    {
            [TestMethod]
            public void MinElement_WhenNumberIsInTheBegining()
            {
                int[] arr1 = { 1, 5, 3, 4, 2 };
                Assert.AreEqual(1, NumberTasks.MinElement(arr1));

               int[] arr2 = { -10, 5, 7, -2, 2 };
               Assert.AreEqual(-10, NumberTasks.MinElement(arr2));

            }

        [TestMethod]
         public void MinElement_ReturnsCorrectMinElement()
         {
               int[] arr1 = { 10, 5, 34, 4, 2 };
               int[] arr2 = { -10, 5, 7, -2, 2 };
               int[] arr3 = { 6 };
               int[] arr4 = {};
                
            int result1 = NumberTasks.MinElement(arr1);
            int result2 = NumberTasks.MinElement(arr2);
            int result3 = NumberTasks.MinElement(arr3);
            int result4 = NumberTasks.MinElement(arr4);

            Assert.AreEqual(2, result1);
            Assert.AreEqual(-10, result2);
            Assert.AreEqual(6, result3);
            Assert.AreEqual(0, result4);
        }
    }

}
