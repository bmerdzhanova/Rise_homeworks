﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmallTasks;

namespace TestSmallTasks
{
    [TestClass]
    public class TestDoubleFact
    {
        [TestMethod]
        public void DoubleFact_WhenNumberLessThanOne_ReturnOne()
        {
            int n = -2;
            int expected = 1;
            long actual = NumberTasks.DoubleFact(n);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void DoubleFact_WhenNumberIsLegit()
        {
            int n = 8;
            int expected = 384;
            long actual = NumberTasks.DoubleFact(n);
            Assert.AreEqual(expected, actual);
        }
    }
}
