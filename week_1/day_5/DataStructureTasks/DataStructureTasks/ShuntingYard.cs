﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructureTasks
{
    public static class ShuntingYard
    {

        public static string ConvertToRPN(Tokenizer tokenizer)
        {
            var result = new List<string>();
            var token = tokenizer.NextToken();
            var stack = new Stack<Tokenizer.Token>();

            while (token.type != Tokenizer.Token.Type.error)
            {
                if (token.type == Tokenizer.Token.Type.number)
                {
                    result.Add(token.numval.ToString());
                }
                else if (token.type == Tokenizer.Token.Type.open_par)
                {
                    stack.Push(token);
                }
                else if (token.type == Tokenizer.Token.Type.close_par)
                {
                    while (stack.Count > 0 && stack.Peek().type != Tokenizer.Token.Type.open_par)
                    {
                        result.Add(stack.Pop().c.ToString());
                    }
                    if (stack.Count == 0)
                    {
                        throw new Exception("Mismatched parentheses");
                    }

                    stack.Pop();
                }
                else if (token.type == Tokenizer.Token.Type.oper)
                {
                    while (stack.Count > 0 && stack.Peek().type == Tokenizer.Token.Type.oper
                           && Priority(stack.Peek().c) >= Priority(token.c))
                    {
                        result.Add(stack.Pop().c.ToString());
                    }
                    stack.Push(token);
                }
                else
                {
                    throw new Exception("Unexpected character");
                }

                token = tokenizer.NextToken();
            }

            while (stack.Count > 0)
            {
                if (stack.Peek().type == Tokenizer.Token.Type.open_par)
                {
                    throw new Exception("Mismatched parentheses");
                }

                result.Add(stack.Pop().c.ToString());
            }

            return string.Join(" ", result);
        }

        private static double Apply(char op, double left, double right)
        {
            switch (op)
            {
                case '+': return left + right;
                case '-': return left - right;
                case '*': return left * right;
                case '/': return left / right;
                default: throw new Exception("Invalid operator " + op);
            }
        }

        private static int Priority(char op)
        {
            switch(op)
            {
                case '+':
                case '-':
                    return 1;
                case '*':
                case '/':
                    return 2;
                default: throw new Exception("Invalid operator " + op);
            }
        }
        public static double EvalRPN(Tokenizer tokenizer)
        {
            var stack = new Stack<double>();
            var token = tokenizer.NextToken();

            while(token.type != Tokenizer.Token.Type.error)
            {
                if(token.type == Tokenizer.Token.Type.number)
                {
                    stack.Push(token.numval);
                }
                else
                {
                    if(stack.Count < 2)
                    {
                        throw new Exception("Not enough operands for operator " + token.c);
                    }

                    double right = stack.Pop();
                    double left = stack.Pop();

                    stack.Push(Apply(token.c, left, right));
                }
                token = tokenizer.NextToken();
            }

            if(stack.Count != 1)
            {
                throw new Exception("Incorrect expression");
            }

            return stack.Peek();
        }

    }
}

