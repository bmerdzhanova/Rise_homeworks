﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DataStructureTasks
{
    public class Tokenizer
    {
        public struct Token
        {
            public enum Type { error, number, open_par, close_par, oper}
            public Type type;
            public double numval;
            public char c;
        }

        private TextReader inStream;

        public Tokenizer(TextReader inStream)
        {
            this.inStream = inStream;
        }

        private void ClearWhiteSpace()
        {
            while(inStream.Peek() >= 0 && inStream.Peek() <= 32)
            {
                inStream.Read();
            }
        }

        public Token NextToken()
        {
            ClearWhiteSpace();

            Token result = new Token { type = Token.Type.error, numval = -1, c = '?' };

            int c = inStream.Peek();
            if(Char.IsDigit((char)c) || c == '.')
            {
                StringBuilder sb = new StringBuilder();
                while (Char.IsDigit((char)c) || c == '.')
                {
                    sb.Append((char)inStream.Read());
                    c = inStream.Peek();
                }
                double numVal;
                if (Double.TryParse(sb.ToString(), out numVal))
                {
                    result.type = Token.Type.number;
                    result.numval = numVal;
                }
                else
                {
                    result.type = Token.Type.error;
                }
            }
            else if (c == '(')
            {
                result.type = Token.Type.open_par;
                result.c = (char)inStream.Read();
            }
            else if (c == ')')
            {
                result.type = Token.Type.close_par;
                result.c = (char)inStream.Read();
            }
            else if(c == '+' || c == '*' || c == '-' || c == '/')
            {
                result.type = Token.Type.oper;
                result.c = (char)inStream.Read();
            }
            else
            {
                inStream.Read();
            }
            return result;
        }

        public bool MoreTokens()
        {
            return inStream.Peek() >= 0;
        }
    }
}
