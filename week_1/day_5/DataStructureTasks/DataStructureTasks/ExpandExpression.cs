﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructureTasks
{
    public class ExpandExpression
    {
        public static string ExpandExp(string exp)
        {
            StringBuilder result = new StringBuilder();
            int i = 0;
            int numOpenParentheses = 0;
            while (i < exp.Length)
            {
                if (char.IsLetter(exp[i]))
                {
                    result.Append(exp[i]);
                    i++;
                }
                else if (char.IsDigit(exp[i]))
                {
                    int count = 0;
                    while (i < exp.Length && char.IsDigit(exp[i]))
                    {
                        count = count * 10 + (exp[i] - '0');
                        i++;
                    }

                    if (i >= exp.Length || exp[i] != '(')
                    {
                        throw new ArgumentException("Missing opening parenthesis.");
                    }

                    numOpenParentheses++;

                    i++;

                    int subExpStart = i;
                    while (i < exp.Length && numOpenParentheses > 0)
                    {
                        if (exp[i] == '(')
                        {
                            numOpenParentheses++;
                        }
                        else if (exp[i] == ')')
                        {
                            numOpenParentheses--;
                        }
                        i++;
                    }

                    if (numOpenParentheses > 0)
                    {
                        throw new ArgumentException("Missing closing parenthesis.");
                    }

                    string subExp = exp.Substring(subExpStart, i - subExpStart - 1);

                    for (int j = 0; j < count; j++)
                    {
                        result.Append(ExpandExp(subExp));
                    }
                }
                else
                {
                    throw new ArgumentException("Invalid character: " + exp[i]);
                }
            }

            if (numOpenParentheses > 0)
            {
                throw new ArgumentException("Missing closing parenthesis.");
            }

            return result.ToString();
        }
    }
}
