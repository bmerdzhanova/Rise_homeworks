﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructureTasks
{
    public class ExpandExpressionWithStack
    {
        public static string ExpandExpWithStacks(string exp)
        {
            if (exp.Length == 0)
            {
                throw new ArgumentException("The length of expression is zero!");
            }
            Stack<int> countStack = new Stack<int>();
            Stack<string> expStack = new Stack<string>();

            int i = 0;
            while (i < exp.Length)
            {
                if (char.IsLetter(exp[i]))
                {
                    expStack.Push(exp[i].ToString());
                    i++;
                }
                else if (char.IsDigit(exp[i]))
                {
                    int count = 0;
                    while (i < exp.Length && char.IsDigit(exp[i]))
                    {
                        count = count * 10 + (exp[i] - '0');
                        i++;
                    }
                    while (i < exp.Length && char.IsWhiteSpace(exp[i]))
                    {
                        i++;
                    }

                    if (i >= exp.Length || exp[i] != '(')
                    {
                        throw new ArgumentException("Missing opening parenthesis.");
                    }

                    countStack.Push(count);
                    expStack.Push("(");
                    i++;
                }
                else if (exp[i] == ')')
                {
                    int count = 1;
                    if (countStack.Count > 0)
                    {
                        count = countStack.Pop();
                    }
                    string subExp = "";
                    while (expStack.Count > 0 && expStack.Peek() != "(")
                    {
                        subExp = expStack.Pop() + subExp;
                    }
                    expStack.Pop();

                    StringBuilder sb = new StringBuilder();
                    for (int j = 0; j < count; j++)
                    {
                        sb.Append(subExp);
                    }
                    expStack.Push(sb.ToString());

                    i++;
                }
                else
                {
                    throw new ArgumentException("Invalid character: " + exp[i]);
                }
            }

            if (countStack.Count > 0)
            {
                throw new ArgumentException("Missing closing parenthesis.");
            }

            StringBuilder result = new StringBuilder();
            while (expStack.Count > 0)
            {
                result.Insert(0, expStack.Pop());
            }

            return result.ToString();
        }

    }
}
