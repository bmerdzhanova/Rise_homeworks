﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeTasks
{
    public class Height
    {
        public int HeightBinaryTree<T>(BinaryTreeNode<T> root)
        {
            if(root == null || 
              (root != null &&  root.LeftNode == null && root.RightNode == null))
            {
                return 0;
            }
            return 1 + Math.Max(HeightBinaryTree<T>(root.LeftNode), HeightBinaryTree<T>(root.RightNode));
        }
    }
}
