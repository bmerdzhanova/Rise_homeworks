﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace TreeTasks
{
    public class BinarySearchTree
    {
        public int MaxValue(BinaryTreeNode<int> root)
        {
            if(root == null)
            {
                return Int32.MinValue;
            }
            int value = root.Value;
            int left = MaxValue(root.LeftNode);
            int right = MaxValue(root.RightNode);

            return Math.Max(value, Math.Max(left, right));
        }

        public int MinValue(BinaryTreeNode<int> root)
        {
            if(root == null)
            {
                return Int32.MaxValue;
            }
            int value = root.Value;
            int left = MinValue(root.LeftNode);
            int right = MinValue(root.RightNode);

            return Math.Min(value, Math.Min(left, right));

        }
        public bool isBST(BinaryTreeNode<int> root)
        {
            if(root == null)
            {
                return true;
            }
            if (MinValue(root.RightNode) < root.Value && root.RightNode != null)
            {
                return false;
            }
            if ( MaxValue(root.LeftNode) > root.Value && root.LeftNode != null)
            {
                return false;
            }
            if (isBST(root.LeftNode) || isBST(root.RightNode))
            {
                return true;
            }
            return true;
        }
    }
}
