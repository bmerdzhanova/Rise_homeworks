﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeTasks
{
    public class Orders
    {
        public void PreOrders<T>(BinaryTreeNode<T> root, List<T> elements)
        {
            if(root != null)
            {
                elements.Add(root.Value); 
                PreOrders<T>(root.LeftNode, elements);
                PreOrders<T>(root.RightNode, elements);
            }
        }

        public void PostOrders<T>(BinaryTreeNode<T> root, List<T> elements)
        {
            if(root == null)
            {
                return;
            }
            PreOrders<T>(root.LeftNode, elements);
            PreOrders<T>(root.RightNode, elements);
            elements.Add(root.Value);
            
        }

        public void InOrders<T>(BinaryTreeNode<T> root, List<T> elements)
        {
            if (root != null)
            {
                PreOrders<T>(root.LeftNode, elements);
                elements.Add(root.Value);
                PreOrders<T>(root.RightNode, elements);

            }
        }
    }
}
