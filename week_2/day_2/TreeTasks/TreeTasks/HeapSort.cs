﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeTasks
{
    public class HeapSort
    {
        public static void Swap(List<int> list, int i, int j)
        {
            int temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }
        public void heapify(List<int> array, int n, int index)
        {
            int largest = index;
            int left = 2 * index + 1;
            int right = 2 * index + 2;

            if(left < n && array[left] > array[largest])
            {
                largest = left;
            }

            if(right < n && array[right] > array[largest])
            {
                largest = right;
            }

            if(largest != index)
            {
                Swap(array, index, largest);

                heapify(array, n, largest);
            }
        }

        public void sortHeap(List<int> array)
        {
            int n = array.Count;
            for(int i = n / 2 - 1; i >= 0; i--)
            {
                heapify(array, n, i);
            }
            for(int i = n - 1; i > 0; i--)
            {
                Swap(array, 0, i);
                heapify(array, i, 0);
            }
        }
    }
}
