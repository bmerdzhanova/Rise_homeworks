﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeTasks
{
    public class KthSmallestElement
    {
        public int kthSmallestWithStack(BinaryTreeNode<int> root, int k)
        {
            if(k == 0)
            {
                throw new ArgumentException("Invalid k value");
            }
            Stack<BinaryTreeNode<int>> elements = new Stack<BinaryTreeNode<int>>();
            while(true)
            {
                while (root != null)
                {
                    elements.Push(root);
                    root = root.LeftNode;
                }
                root = elements.Peek();
                elements.Pop();
                if(--k == 0)
                {
                    return root.Value;
                }
                root = root.RightNode;
            }
        }
    }
}
