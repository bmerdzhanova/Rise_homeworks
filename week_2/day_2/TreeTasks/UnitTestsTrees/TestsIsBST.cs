﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreeTasks;

namespace UnitTestsTrees
{
    [TestClass]
    public class TestsIsBST
    {
        [TestMethod]
        public void TestIsBST_WhenInputWithIntegers_ReturnTrue()
        {
            BinaryTreeNode<int> node60 = new BinaryTreeNode<int>(60, null, null);

            BinaryTreeNode<int> node45 = new BinaryTreeNode<int>(45, null, null);
            BinaryTreeNode<int> node35 = new BinaryTreeNode<int>(35, null, null);
            BinaryTreeNode<int> node25 = new BinaryTreeNode<int>(25, null, null);

            BinaryTreeNode<int> node50 = new BinaryTreeNode<int>(50, node45, node60);
            BinaryTreeNode<int> node30 = new BinaryTreeNode<int>(30, node25, node35);
            BinaryTreeNode<int> tree = new BinaryTreeNode<int>(40, node30, node50);

            var BST = new BinarySearchTree();

            var result = BST.isBST(tree);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestIsBST_WhenInputWithIntegers_ReturnFalse()
        {
            BinaryTreeNode<int> node60 = new BinaryTreeNode<int>(60, null, null);

            BinaryTreeNode<int> node45 = new BinaryTreeNode<int>(45, null, null);
            BinaryTreeNode<int> node35 = new BinaryTreeNode<int>(35, null, null);
            BinaryTreeNode<int> node25 = new BinaryTreeNode<int>(25, null, null);

            BinaryTreeNode<int> node2 = new BinaryTreeNode<int>(2, node45, node60);
            BinaryTreeNode<int> node30 = new BinaryTreeNode<int>(30, node25, node35);
            BinaryTreeNode<int> tree = new BinaryTreeNode<int>(40, node30, node2);

            var BST = new BinarySearchTree();

            var result = BST.isBST(tree);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestIsBST_WhenInputIsEmptyTree()
        {
            BinaryTreeNode<int> tree = null;

            var BST = new BinarySearchTree();

            var result = BST.isBST(tree);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestIsBST_WhenInputIsTreeWithOneElement()
        {
            BinaryTreeNode<int> tree = new BinaryTreeNode<int>(5, null, null);

            var BST = new BinarySearchTree();

            var result = BST.isBST(tree);

            Assert.IsTrue(result);
        }
    }
}
