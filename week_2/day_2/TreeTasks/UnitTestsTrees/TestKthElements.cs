﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreeTasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace UnitTestsTrees
{
    [TestClass]
    public class TestKthElements
    {
        public BinaryTreeNode<int> ConstructTree()
        {
            BinaryTreeNode<int> node9 = new BinaryTreeNode<int>(76, null, null);
            BinaryTreeNode<int> node8 = new BinaryTreeNode<int>(61, null, null);
            BinaryTreeNode<int> node7 = new BinaryTreeNode<int>(35, null, null);

            BinaryTreeNode<int> node6 = new BinaryTreeNode<int>(69, node8, node9);
            BinaryTreeNode<int> node5 = new BinaryTreeNode<int>(38, node7, null);
            BinaryTreeNode<int> node4 = new BinaryTreeNode<int>(24, null, null);

            BinaryTreeNode<int> node3 = new BinaryTreeNode<int>(52, node5, node6);
            BinaryTreeNode<int> node2 = new BinaryTreeNode<int>(11, null, node4);
            BinaryTreeNode<int> node1 = new BinaryTreeNode<int>(25, node2, node3);

            return node1;
        }
        [TestMethod]
        public void TestKthElement_WithkEqual5()
        {
            var KthSmallestElement = new KthSmallestElement();
            BinaryTreeNode<int> tree = ConstructTree();
            int k = 5;
            int actual = 38;
            int expected = KthSmallestElement.kthSmallestWithStack(tree, k);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestKthElement_WithkEqual0()
        {
            var KthSmallestElement = new KthSmallestElement();
            BinaryTreeNode<int> tree = ConstructTree();
            int k = 0;
            Assert.ThrowsException<ArgumentException>(() => KthSmallestElement.kthSmallestWithStack(tree, k));
        }
        [TestMethod]
        public void TestKthElement_WithkEqual1()
        {
            var KthSmallestElement = new KthSmallestElement();
            BinaryTreeNode<int> tree = ConstructTree();
            int k = 1;
            int actual = 11;
            int expected = KthSmallestElement.kthSmallestWithStack(tree, k);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestKthElement_WithkEqualCountNodeInTree()
        {
            var KthSmallestElement = new KthSmallestElement();
            BinaryTreeNode<int> tree = ConstructTree();
            int k = 9;
            int actual = 76;
            int expected = KthSmallestElement.kthSmallestWithStack(tree, k);
            Assert.AreEqual(expected, actual);
        }
    }
}
