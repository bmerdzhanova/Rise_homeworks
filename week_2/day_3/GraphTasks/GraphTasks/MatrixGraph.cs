﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphTasks
{
    public class MatrixGraph
    {
        public int VertexCount { get; set; }
        public List<List<int>> adjMatrix { get; set; }
        public List<bool> Visited { get; set; }
        public MatrixGraph(int size)
        {
            VertexCount = size;
            InitMatrix();
        }

        private void InitMatrix()
        {
            adjMatrix = new List<List<int>>(VertexCount);
            for (int rowIndex = 0; rowIndex < VertexCount; rowIndex++)
            {
                adjMatrix.Add(new List<int>(VertexCount));
                for (int colIndex = 0; colIndex < VertexCount; colIndex++)
                {
                    adjMatrix[rowIndex].Add(0);
                }
            }
        }

        public void AddEdge(int startNodeIndex, int endNodeIndex)
        {
            adjMatrix[startNodeIndex][endNodeIndex] = 1;
            adjMatrix[endNodeIndex][startNodeIndex] = 1;
        }

        public void DFS(int startNodeIndex)
        {
            Visited = new List<bool>(VertexCount);
            for (int vertexIndex = 0; vertexIndex < VertexCount; vertexIndex++)
            {
                Visited.Add(false);
            }

            DFSInternal(startNodeIndex);
        }

        private void DFSInternal(int startNodeIndex)
        {
            Console.Write(startNodeIndex + " ");
            Visited[startNodeIndex] = true;

            for (int colIndex = 0; colIndex < VertexCount; colIndex++)
            {
                if (adjMatrix[startNodeIndex][colIndex] == 1 && !Visited[colIndex])
                {
                    DFSInternal(colIndex);
                }
            }
        }
    }

}
