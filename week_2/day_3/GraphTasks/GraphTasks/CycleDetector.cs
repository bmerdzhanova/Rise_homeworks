﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphTasks
{
    public class CycleDetector
    {
        public bool HasCycle(int start, MatrixGraph graph)
        {
            var orderedVertexes = new Stack<int>();
            var visited = new HashSet<int>();

            orderedVertexes.Push(start);

            while (orderedVertexes.Count > 0)
            {
                var current = orderedVertexes.Pop();
                if (!visited.Add(current))
                {
                    return true;
                }

                visited.Add(current);

                for (int i = 0; i < graph.VertexCount; i++)
                {
                    if (graph.adjMatrix[current][i] == 1 && !visited.Contains(i))
                    {
                        orderedVertexes.Push(i);
                    }
                }
            }

            return false;
        }
    }
}
