﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphTasks
{
    public class HamiltonianCycle
    {
        public bool HasHamiltonianCycle(MatrixGraph graph)
        {
            graph.Visited = new List<bool>(graph.VertexCount);
            for(int i = 0; i < graph.VertexCount; i++)
            {
                graph.Visited.Add(false);
            }

            for(int startNodeIndex = 0; startNodeIndex < graph.VertexCount; startNodeIndex++)
            {
                List<int> path = new List<int>();
                path.Add(startNodeIndex);
                if(FindHamiltonianCycle(path, graph))
                {
                    return true;
                }
            }
            return false;
        }

        private bool FindHamiltonianCycle(List<int> path, MatrixGraph graph)
        {
            int lastNode = path[path.Count - 1];
            if(path.Count == graph.VertexCount)
            {
                return graph.adjMatrix[lastNode][path[0]] == 1;
            }

            bool cycleFound = false;
            for(int nextNode = 0; nextNode < graph.VertexCount; nextNode++)
            {
                if (graph.adjMatrix[lastNode][nextNode] == 1 && !graph.Visited[nextNode])
                {
                    graph.Visited[nextNode] = true;
                    path.Add(nextNode);
                    cycleFound = FindHamiltonianCycle(path, graph);
                    if(cycleFound)
                    {
                        break;
                    }
                    graph.Visited[nextNode] = false;
                    path.RemoveAt(path.Count - 1);
                }
            }
            return cycleFound;
        }
    }
}