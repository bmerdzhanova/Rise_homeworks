﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphTasks
{
    public class AdjListGraph
    {
        private int VertexCount;
        Dictionary<int, List<int>> data;

        public AdjListGraph(int vertexCount)
        {
            data = new Dictionary<int, List<int>>();
            VertexCount = vertexCount;
            for (int i = 0; i < vertexCount; ++i)
            {
                data.Add(i, new List<int>());
            }
        }
        
        public void addEdge(int startNodeIndex, int endNodeIndex)
        {
            data[startNodeIndex].Add(endNodeIndex);
            data[endNodeIndex].Add(startNodeIndex);
        }
    }
}
