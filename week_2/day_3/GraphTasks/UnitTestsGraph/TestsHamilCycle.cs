global using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphTasks;
namespace UnitTestsGraph
{
    [TestClass]
    public class HamiltonianCycleTests
    {
        private MatrixGraph graph1;
        private MatrixGraph graph2;

        [TestInitialize]
        public void SetUp()
        {
            graph1 = new MatrixGraph(3);
            graph1.AddEdge(0, 1);
            graph1.AddEdge(1, 2);
            graph1.AddEdge(0, 2);

            graph2 = new MatrixGraph(3);
            graph2.AddEdge(0, 1);
            graph2.AddEdge(1, 0);
        }

        [TestMethod]
        public void TestHasHamiltonianCycle_Example1_ReturnsTrue()
        {
            var cycle = new HamiltonianCycle();
            bool hasCycle = cycle.HasHamiltonianCycle(graph1);

            Assert.IsTrue(hasCycle);
        }

        [TestMethod]
        public void TestHasHamiltonianCycle_Example2_ReturnsFalse()
        {
            var cycle = new HamiltonianCycle();
            bool hasCycle = cycle.HasHamiltonianCycle(graph2);

            Assert.IsFalse(hasCycle);
        }
    }
}