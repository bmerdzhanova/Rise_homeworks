﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace HashingTasks
{
    public class UserAccount
    {
        public string name { get; set; }
        public byte[] password { get; set; }

        
        public UserAccount(string name, string password)
        {
            this.name = name;
            this.password = CalculateSHA256(password);
        }

        public byte[] CalculateSHA256(string str)
        {
            SHA256 sha256 = SHA256.Create();
            byte[] hashValue;
            UTF8Encoding objUtf8 = new UTF8Encoding();
            hashValue = sha256.ComputeHash(objUtf8.GetBytes(str));

            return hashValue;
        }
    }
}
