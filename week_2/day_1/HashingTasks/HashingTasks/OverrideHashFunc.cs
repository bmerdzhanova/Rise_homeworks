﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashingTasks
{
    public class OverrideHashFunc
    {
        public class Person
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public int Age { get; set; }


            public override int GetHashCode()
            {
                unchecked
                {
                    int hash = 22;
                    hash = hash * 6 + FirstName.GetHashCode();
                    hash = hash * 6 + LastName.GetHashCode(); ;
                    hash = hash * 6 + Age.GetHashCode();
                    return hash;
                }

            }
        }
    }
}
