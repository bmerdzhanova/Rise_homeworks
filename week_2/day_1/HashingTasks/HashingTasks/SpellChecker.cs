﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashingTasks
{
    public class SpellChecker
    {
        public static HashSet<string> SpellCheck(HashSet<string> dictionary, string document)
        {
            string[] words = document.Split(' ', '.', ',', ';', ':', '!', '?');
           HashSet<string> result = new HashSet<string>();
            foreach(string word in words)
            {
                if(!dictionary.Contains(word.ToLower()) && !result.Contains(word.ToLower()))
                {
                    result.Add(word);
                }
            }
            return result;
        }
    }
}
