﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace HashingTasks
{
    public class Intersection
    {
        public static List<T> FindIntersection<T>(List<T> array1, List<T> array2)
        {
            HashSet<T> set = new HashSet<T>();
            List<T> intersection = new List<T>();
            foreach(T item in array1)
            {
                set.Add(item);
            }
            foreach(T item in array2)
            {
                if(set.Add(item) == false)
                {
                    intersection.Add(item);
                }
            }
            return intersection;
        }
    }
}
