﻿using HashingTasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestsHashing
{
    [TestClass]
    public class TestsOverrideFunc
    {
        [TestMethod]
        public void GetHashCode_ShouldReturnSameHashCodeForEqualObjects()
        {
            var person1 = new OverrideHashFunc.Person { FirstName = "Viki", LastName = "Ivanova", Age = 22 };
            var person2 = new OverrideHashFunc.Person { FirstName = "Viki", LastName = "Ivanova", Age = 22 };

            var hash1 = person1.GetHashCode();
            var hash2 = person2.GetHashCode();

            Assert.AreEqual(hash1, hash2);
        }

        [TestMethod]
        public void GetHashCode_ShouldReturnDifferentHashCodeForDifferentObjects()
        {
            var person1 = new OverrideHashFunc.Person { FirstName = "Viki", LastName = "Ivanova", Age = 22 };
            var person2 = new OverrideHashFunc.Person { FirstName = "Hristo", LastName = "Ivanov", Age = 18 };

            var hash1 = person1.GetHashCode();
            var hash2 = person2.GetHashCode();

            Assert.AreNotEqual(hash1, hash2);
        }
    }
}
