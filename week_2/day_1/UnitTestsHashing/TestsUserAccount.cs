﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HashingTasks;
using Newtonsoft.Json.Linq;
using static System.Net.Mime.MediaTypeNames;
using System.Reflection.Metadata;

namespace UnitTestsHashing
{
    [TestClass]
    public class TestsUserAccount
    {
        [TestMethod]
        public void UserAccount_Constructor_ShouldSetProperties()
        {
            string name = "Viki";
            string password = "my_password";

            var user = new UserAccount(name, password);

            Assert.AreEqual(name, user.name);
            Assert.IsNotNull(user.password);
        }

        [TestMethod]
        public void UserAccount_Constructor_ShouldHandleEmptyInputs()
        {
            string name = "";
            string password = "";

            var user = new UserAccount(name, password);

            Assert.AreEqual(name, user.name);
            Assert.IsNotNull(user.password);
        }

        [TestMethod]
        public void UserAccount_Constructor_ShouldHandleEmptyPassword()
        {
            string name = "Viki";
            string password = "";

            var user = new UserAccount(name, password);

            Assert.AreEqual(name, user.name);
            Assert.IsNotNull(user.password);
        }

        [TestMethod]
        public void UserAccount_Constructor_ShouldCalculateSHA256()
        {
            string name = "Viki";
            string password = "my_password";
            byte[] expectedHashValue = new byte[] { 0xf6, 0xe2, 0x48, 0xea, 0x99, 0x4f, 0x3e, 0x34, 0x2f, 0x61, 0x14, 0x1b, 0x8b, 0x8e, 0x3e, 0xde, 0x86, 0xd4, 0xde, 0x53, 0x25, 0x7a, 0xbc, 0x8d, 0x06, 0xae, 0x07, 0xa1, 0xda, 0x73, 0xfb, 0x39 };

            var user = new UserAccount(name, password);

            Assert.AreEqual(name, user.name);
            Assert.IsNotNull(user.password);
            CollectionAssert.AreEqual(expectedHashValue, user.password);
        }

    }
}
