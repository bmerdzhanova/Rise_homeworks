using Microsoft.VisualStudio.TestTools.UnitTesting;
using HashingTasks;
namespace UnitTestsHashing
{
    [TestClass]
    public class TestIntersection
    {
        [TestMethod]
        public void TestIntersection_WhenInputIsWithInt()
        {
            List<int> array1 = new List<int> { 1, 2, 3, 4 };
            List<int> array2 = new List<int> { 1, 2, 3, 5 };
            List<int> expected = new List<int> { 1, 2, 3 };
            List<int> actual = Intersection.FindIntersection(array1, array2);
            CollectionAssert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestIntersection_WhenArraysAreEqual()
        {
            List<int> array1 = new List<int> { 1, 2, 3, 4 };
            List<int> array2 = new List<int> { 1, 2, 3, 4 };
            List<int> expected = new List<int> { 1, 2, 3, 4 };
            List<int> actual = Intersection.FindIntersection(array1, array2);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestIntersection_WhenArraysNoIntersection()
        {
            List<int> array1 = new List<int> { 1, 2, 3, 4 };
            List<int> array2 = new List<int> { 5, 6, 7 };
            List<int> expected = new List<int> { };
            List<int> actual = Intersection.FindIntersection(array1, array2);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestIntersection_WhenArraysWithNegativeElement()
        {
            List<int> array1 = new List<int> { -1, 2, -3, 4 };
            List<int> array2 = new List<int> { 5, -6, -3 };
            List<int> expected = new List<int> {-3 };
            List<int> actual = Intersection.FindIntersection(array1, array2);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestIntersection_WhenInputIsWithChars()
        {
            List<int> array1 = new List<int> { 'a', 'b', 'c'};
            List<int> array2 = new List<int> { 'b', 'c' };
            List<int> expected = new List<int> { 'b', 'c' };
            List<int> actual = Intersection.FindIntersection(array1, array2);
            CollectionAssert.AreEqual(expected, actual);
        }
    }

}